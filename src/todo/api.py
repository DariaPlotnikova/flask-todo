"""Обработчики запросов."""
from flask import abort
from flask import request

from . import app
from .constants import API_PREFIX
from .services import create_todo
from .services import delete_todo
from .services import get_todo
from .services import get_todos
from .services import update_todo
from .utils import list_to_json
from .utils import object_to_json


_app_prefix = f'{API_PREFIX}/todo'


@app.route(_app_prefix, methods=['GET', ])
def todos():
    todo_list = get_todos()
    return dict(data=list_to_json(todo_list))


@app.route(_app_prefix, methods=['POST', ])
def create():
    request_data = request.get_json()
    creation_params = dict(
        title=request_data.get('title'),
        description=request_data.get('description', ''),
        date_to=request_data.get('date_to'),
    )
    todo = create_todo(creation_params)
    return dict(data=object_to_json(todo))


@app.route(f'{_app_prefix}/<todo_id>', methods=['GET', ])
def todo(todo_id):
    todo = get_todo(todo_id)
    if not todo:
        abort(404)
    return dict(data=object_to_json(todo))


@app.route(f'{_app_prefix}/<todo_id>', methods=['DELETE', ])
def delete(todo_id):
    todo = get_todo(todo_id)
    if not todo:
        abort(404)
    delete_todo(todo)
    return dict(data=object_to_json(todo))


@app.route(f'{_app_prefix}/<todo_id>', methods=['PUT', ])
def update(todo_id):
    todo = get_todo(todo_id)
    if not todo:
        abort(404)
    update_todo(todo, request.get_json())
    return dict(data=object_to_json(todo))
