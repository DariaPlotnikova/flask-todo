from typing import Iterable
from typing import List

from .database import Todo


def object_to_json(todo: Todo) -> dict:
    return dict(
        id=todo.id,
        title=todo.title,
        description=todo.description,
        date_to=todo.date_to,
    )


def list_to_json(todos: Iterable[Todo]) -> List[dict]:
    return [object_to_json(obj) for obj in todos]
