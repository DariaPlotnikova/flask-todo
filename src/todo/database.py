from contextlib import contextmanager

from sqlalchemy.exc import IntegrityError

from . import app
from . import db


debug = app.logger.debug


class Todo(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=False)
    description = db.Column(db.String(300), unique=False, nullable=True)
    updated_at = db.Column(db.DateTime)
    date_to = db.Column(db.DateTime)

    def __repr__(self):
        return f'{self.title}'


@contextmanager
def autocommit():
    try:
        yield db.session
        db.session.commit()
    except IntegrityError as exc:
        debug('Не удалось обновить БД: ', exc.args)
        db.session.rollback()
