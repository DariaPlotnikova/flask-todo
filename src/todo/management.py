"""Команды управления приложением."""
from psycopg2 import OperationalError as PgOperationalError

from sqlalchemy.exc import OperationalError
from sqlalchemy_utils import create_database
from sqlalchemy_utils import database_exists
from sqlalchemy_utils import drop_database

from . import app
from . import db


@app.cli.command('create_db')
def create_db():
    """Создает БД и таблицы."""
    db_url = db.app.config.get('SQLALCHEMY_DATABASE_URI')
    try:
        if database_exists(db_url):
            drop_database(db_url)
    except (PgOperationalError, OperationalError):
        pass
    create_database(db_url)
    db.create_all()
