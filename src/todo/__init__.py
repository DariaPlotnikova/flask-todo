from importlib import import_module
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


_config = {
    'SECRET_KEY': os.environ.get('TODO_SECRET_KEY'),
    'DATABASE_URL': os.environ.get(
        'TODO_DATABASE_URL',
        'postgresql://todouser:todopass@db/todouser'
    ),
}


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = _config['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


import_module(f'{__package__}.api')
import_module(f'{__package__}.management')
