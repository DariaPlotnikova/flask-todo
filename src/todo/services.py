"""Модуль для функций и классов, реализующих бизнес-логику."""
from datetime import datetime

from .database import Todo
from .database import autocommit


def get_todo(obj_id: int):
    return Todo.query.filter_by(id=obj_id).first()


def get_todos():
    return Todo.query.order_by(Todo.date_to.desc())


def create_todo(obj_params: dict) -> Todo:
    with autocommit() as session:
        todo = Todo(**obj_params, updated_at=datetime.now())
        session.add(todo)
        return todo


def delete_todo(obj: Todo):
    with autocommit() as session:
        session.delete(obj)
        return obj


def update_todo(obj: Todo, new_params: dict) -> Todo:
    object_attr = obj.__dict__
    with autocommit() as session:
        for attr, value in new_params.items():
            if attr in object_attr and attr != 'id':
                setattr(obj, attr, value)
        obj.updated_at = datetime.now()
        session.add(obj)
        return obj
