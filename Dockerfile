FROM ubuntu:latest

RUN apt-get update && apt-get install -y \
    python-dev \
    build-essential \
    python3-pip

COPY . /app
WORKDIR /app/src
RUN cd /app/src

RUN pip3 install -r ../requirements/dev.txt
